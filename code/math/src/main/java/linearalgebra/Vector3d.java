package linearalgebra;
//Joel S Molina B   2235364

/**
 * Vector3d is a class used to calculate the magnitude and dot products of (a) vector(s)
 * information about Vector3d
 * @author  Joel Sebastian Molina Barbosa
 * @version 04/10/2023
 * */

public class Vector3d {
    protected double x;
    protected double y;
    protected double z;
    /**
     * sets the x, y and z fields
     * 
     * @param x value for x
     * @param y value for y
     * @param z value for z
     */
    public Vector3d(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    /**
     * returns the value of the x field
     * 
     * @return the value of x
     */
    public double getX() {
        return this.x;
    }

    /**
     * returns the value of the y field
     * 
     * @return value of y
     */
    public double getY() {
        return this.y;
    }

    /**
     * returns the value of the z field
     * 
     * @return value of z
     */
    public double getZ() {
        return this.z;
    }

    /**
     * Overrides the toString() method to print a new custom interpretation of the object
     */
    @Override
    public String toString() {
        return "X: " + this.x + "\nY: " + this.y + "\nZ: " + this.z;
    }

    /**
     * determines the magnitude of the vector object (x^2+y^2+z^2)
     * 
     * @return the result of x^2+y^2+z^2
     */
    public double magnitude() {
        return Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2) + Math.pow(z, 2));
    }

    /**
     * calculates the dot product of two vectors
     * 
     * @param otherV the other vector object to multioply into the main one
     * @return the result of x*otherX+y*otherY+z*otherZ
     */
    public double dotProduct(Vector3d otherV) {
        return otherV.x * this.x + otherV.y * this.y + otherV.z * this.z;
    }

    /**
     * returns the sum of two vectors
     * 
     * @param otherV the vector to sum into our vector
     * @return the resulting new vector with field values of the sum of our two vectors
     */
    public Vector3d add(Vector3d otherV) {
        otherV.x += this.x;
        otherV.y += this.y;
        otherV.z += this.z;
        return otherV;
    }

    /**
     * Compares if two vector objects have the same values
     * 
     * @return a boolean indicating if the vectors are equals(true) or not (false)
     */
    public boolean equals(Object o) {
        Vector3d e = (Vector3d) o;
        if(e.x == this.x && e.y == this.y && e.z == this.z) {
           return true;
        }
        return false;
    }
}
